using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

// ��������� � https://assetstore.unity.com/packages/essentials/starter-assets-third-person-character-controller-196526

namespace TestDino {
	public class Inputs : MonoBehaviour {
		[Header("Character Input Values")]
		public Vector2 move;
		public Vector2 look;
		public bool jump;
		public bool sprint;

		[Header("Movement Settings")]
		public bool analogMovement;

#if !UNITY_IOS || !UNITY_ANDROID
		[Header("Mouse Cursor Settings")]
		public bool cursorLocked = true;
		public bool cursorInputForLook = true;
#endif

#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
		// �� ����� StarterAssets, ������ �� SendMessage. �������� ��� �������.
		public void OnMove(InputValue value) {
			MoveInput(value.Get<Vector2>());
		}
		public void OnLook(InputValue value) {
			if (cursorInputForLook) {
				LookInput(value.Get<Vector2>());
			}
		}
		public void OnJump(InputValue value) {
			JumpInput(value.isPressed);
		}
		public void OnSprint (InputValue value) {
			SprintInput(value.isPressed);
		}
		// ������ �� UnityEvent
		public void OnMoveEvent (InputAction.CallbackContext context) {
			MoveInput(context.ReadValue<Vector2>());
		}
		public void OnJumpEvent (InputAction.CallbackContext context) {
			bool res = context.action.triggered && context.action.ReadValue<float>() > 0;
			JumpInput(res);
		}
		public void OnSprintEvent (InputAction.CallbackContext context) {
			SprintInput(context.action.IsPressed());
		}
#else
	// old input sys if we do decide to have it (most likely wont)...
#endif
		public void MoveInput(Vector2 newMoveDirection) {
			move = newMoveDirection;
		} 

		public void LookInput(Vector2 newLookDirection) {
			look = newLookDirection;
		}

		public void JumpInput(bool newJumpState) {
			jump = newJumpState;
		}

		public void SprintInput(bool newSprintState) {
			sprint = newSprintState;
		}

#if !UNITY_IOS || !UNITY_ANDROID

		private void OnApplicationFocus(bool hasFocus) {
			SetCursorState(cursorLocked);
		}

		private void SetCursorState(bool newState) {
			Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
		}

#endif

	}
	
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

// ��������: � ������ �������� InputSystem ��������� �� ������ ����� ������ �������� (���). ��� ������������� � ������ ������ ���������� PlayerInput, ��� �� �����.
// �������: ������� ��� ������ PlayerInput
// ������: ���� ���-�� ����������� PlayerInput, ����� ��� �������������� ���������. ��������, ����, ��� ���� ������ � ���������.
//
// �����, ��� ���� ������� ��������� ��������� PlayerInput, � ����� �������� ������, �� �� �������� ������.
// ��� ����� �������� ������ ����� �����, ���� ���� ��� ��������� � ����� � ��� �� ��������� (��� ��� ����, ��������)
// ������ ������� ����� - ���� ������� ����, ��� ��������� ���������.
// ������, ������������ �����, ���� �� ���������� ��������, �� �������� �������� ������ PLayerInput - ��� �� ������ ������� �� ������.
// ���� �� ����, �� ������ ������������.

namespace TestDino {

	public class ActivateInput : MonoBehaviour {

		PlayerInput[] inputs; 
		public float latency = 0.1f; // ��-���� ����� ��������, ����� ���������� ��� �� ����� �� ����� PlayerInput

		void Start () {
			inputs = FindObjectsOfType<PlayerInput>();
		}

		IEnumerator InFrameActivator (PlayerInput input) {
			foreach (PlayerInput pi in inputs) pi.enabled = false;
			yield return new WaitForSeconds(latency);
			foreach (PlayerInput pi in inputs) if (pi == input) pi.enabled = true;
		}

		public void ActivateThis (PlayerInput input) {
			StartCoroutine(InFrameActivator(input));
		}

	}

}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TestDino {

	public class DamageReceiver : MonoBehaviour {

		[Tooltip("������������ ���������")]
		public float durabilityMax = 4; // N of hits
		[HideInInspector]
		public float durability;
		[Tooltip("�������� �� ���������� ������ ���������")]
		public UnityEvent actions;

		void Start () {
			durability = durabilityMax;
		}

		private void OnCollisionEnter (Collision collision) {
			durability -= 1;
			if (durability == 0) {
				actions.Invoke();
			}
		}

	}

}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// ���������� ������ ������. ���������� ������ ���������������� �� ���������� �������� � ��������.
// ������ ���� ��� ����� ������ ���������� � ��������. ��� ������ ������� ���������, ���������� ������.

public class EnemyGangController : MonoBehaviour {

	[Header("������� ������ ������������� ����� ��������")]
	public int count; // ������ ��� �������
	public UnityEvent actions;

	public float gizmoRadius = 0.2f;
	public float gizmoShiftUp = 0.5f;

	private void Start () {
		count = transform.childCount;
	}

	public void Dec () {
		if (count > 0) { // ������ �� ���������� ����, � �� ���� ��...
			count -= 1;
			if (count == 0) actions.Invoke();
		}
	}

	void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position + Vector3.up * gizmoShiftUp, gizmoRadius);
	}


}

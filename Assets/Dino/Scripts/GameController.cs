using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace TestDino {

	public class GameController : MonoBehaviour {

		public UnityEvent firstTapActions; // ���������� ��� ������ ���� �� ������

		bool firstTap = false;

		public void Tap (InputAction.CallbackContext context) {
			if (!firstTap) {
				firstTap = true;
				firstTapActions.Invoke();
			}
		}

		public void ReloadScene () {
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}

	}

}
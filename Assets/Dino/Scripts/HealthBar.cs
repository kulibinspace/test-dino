using UnityEngine;

namespace TestDino {
	
	// https://www.stevestreeting.com/2019/02/22/enemy-health-bars-in-1-draw-call-in-unity/
	// 14:55 01.10.2019 полоска здоровья врага. На старте компонент рендерера должен быть выключен. Включается один раз, при первом попадании.
	
	public class HealthBar : MonoBehaviour {

		MaterialPropertyBlock matBlock;
		MeshRenderer meshRenderer; 
		public DamageReceiver dr;
		bool first = true;

		private void Awake() {
			meshRenderer = GetComponent<MeshRenderer>();
			matBlock = new MaterialPropertyBlock();
		}

		private void Update() {
			if (dr.durability <= dr.durabilityMax) {
				if (first) { meshRenderer.enabled = true; first = false; }
				AlignCamera();
				UpdateParams();
				if (dr.durability == 0) gameObject.SetActive(false);
			}
		}

		private void UpdateParams() {
			meshRenderer.GetPropertyBlock(matBlock);
			matBlock.SetFloat("_Fill", dr.durability / dr.durabilityMax);
			meshRenderer.SetPropertyBlock(matBlock);
		}

		private void AlignCamera() {
			var camXform = Camera.main.transform;
			var forward = transform.position - camXform.position;
			forward.Normalize();
			var up = Vector3.Cross(forward, camXform.right);
			transform.rotation = Quaternion.LookRotation(forward, up);
		}

	}
	
}
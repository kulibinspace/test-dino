using UnityEngine;
using System.Collections;
using TestDino;

public class MoveTo : MonoBehaviour {

    UnityEngine.AI.NavMeshAgent agent;
    private CharacterController controller;
    private float animationBlend;
    public Animator animator;

	// animation IDs
	private int _animIDSpeed;
	private int _animIDMotionSpeed;

	private void AssignAnimationIDs () {
		_animIDSpeed = Animator.StringToHash("Speed");
		_animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
	}

    void Start () {
        AssignAnimationIDs();
        controller = GetComponent<CharacterController>();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    public void NextGoal (GameObject goalObject) {
        agent.destination = goalObject.transform.position;
	}

	private void Update () {
        // �� ����� ������� ������ � ����������.
        // ���� ������� ��� � https://docs.unity3d.com/ru/530/Manual/nav-CouplingAnimationAndNavigation.html
        controller.Move(agent.velocity * Time.deltaTime);
        animationBlend = Mathf.Lerp(animationBlend, agent.velocity.magnitude, Time.deltaTime);
		animator.SetFloat(_animIDSpeed, animationBlend);
		animator.SetFloat(_animIDMotionSpeed, 1); // 1 �� ������, ���� �� ����, ��� ��������� � ���������� �������� � blendtree
	}

}
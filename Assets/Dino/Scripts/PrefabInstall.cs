﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TestDino {

	public class PrefabInstall : MonoBehaviour {

		public GameObject prefab;
		public bool onAwake = true;
		public bool onEnable = false;
		[Tooltip("Оставить префабу его исходную трансформацию")]
		public bool sourceTransform = false;
		[Tooltip("сделать родителем этот объект")]
		public bool parent = true;
		[Tooltip("сработать автоматически при отцеплении от родителя")]
		public bool autoOnDetach = false;
		public UnityEvent actions;
		private GameObject clone;

		void Awake () {
			if (onAwake) Install();
		}

		void OnEnable () {
			if (onEnable) Install();
		}

		public void Install () {
			Transform p = null; if (parent) p = transform;
			if (sourceTransform) 
				clone = Instantiate(prefab, p);
			else {
				clone = Instantiate(prefab, transform.position, transform.rotation, p);
				clone.transform.localScale = Vector3.Scale(clone.transform.lossyScale, transform.lossyScale);
			}
			actions.Invoke();
		}
	
		void OnTransformParentChanged () {
			if (autoOnDetach) Install();
		}
	
		public void Uninstall () {
			if (clone) Destroy(clone);
		}
	}
}

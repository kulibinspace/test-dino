using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

// ������� ������� � ��� ��� ����� ��� ���-������
namespace TestDino {

	public class ReturnToPool : MonoBehaviour {

		private void OnCollisionEnter (Collision collision) {
			transform.rotation = Quaternion.identity;
			GetComponent<Rigidbody>().velocity = Vector3.zero;
			EasyObjectPool.instance.ReturnObjectToPool(gameObject);
		}

	}

}

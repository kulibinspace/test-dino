using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using MarchingBytes;

namespace TestDino {

	public class Shoot : MonoBehaviour {

		[Tooltip("��������� ������� ������")]
		public Transform shootPoint; // ��������� ������� �������, ���-�� �� ����� ����� � ����������
		[Tooltip("�������� ������ �����")]
		public float shootSpeed = 10f;
		[Tooltip("���� �������� ����� ��� ������")]
		public float twist = 10f;
		[Tooltip("��� ���� � �������")]
		public string projectilePoolName = "Stick";

		public void Do (InputAction.CallbackContext context) {
			if (context.action.WasPerformedThisFrame()) {
				// ������� �� ���������, �.�. �� ������������ ��� �������� � ���.
				GameObject newObject = EasyObjectPool.instance.GetObjectFromPool(projectilePoolName, shootPoint.position, transform.rotation);
				Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
				Vector3 distantPoint = ray.origin + ray.direction * 100; // 100 ������ ������ ����� ������ ���� ����������
				RaycastHit hit;
				// ����� ������ �������������� �� ����������, � ������� ��������� ���
				if (Physics.Raycast(ray, out hit)) {
					distantPoint = hit.point;
				}
				// ������ ������ ������ ���� ��������� �� ���������, ����� ����� ��������� ���������
				Vector3 shootVec = (distantPoint - shootPoint.position).normalized;
				//Debug.DrawRay(shootPoint.position, shootVec, Color.red, 10);
				Vector3 torque = Vector3.Cross(Vector3.up, shootVec).normalized;
				newObject.GetComponent<Rigidbody>().AddForce(shootVec * shootSpeed, ForceMode.VelocityChange);
				newObject.GetComponent<Rigidbody>().AddTorque(torque * twist);
			}
		}

	}

}

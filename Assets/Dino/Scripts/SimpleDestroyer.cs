using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestDino {

	public class SimpleDestroyer : MonoBehaviour {

		public void JustDestroy () {
			Destroy(gameObject);
		}

	}

}
